/**
 * File Name	:	config.js
 * Description:	config file.
 * Author		  :	Bhuvaneshwari B
 * Date			  :	Aug 02, 2020
 * 
 * 
 * 
 */


// Module dependencies.
var express = require('express'),
    http = require('http'), 
    path = require('path'),
    mongoose = require('mongoose');

// Express Initialization
var app = express();
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);

// all environments
app.set('port', process.env.PORT || 1995);
app.set('view engine', 'ejs');


// Database Connection
var server = http.createServer(app);
var dbConnect = require('./server/config/dbService.js');
var connection = mongoose.connect(dbConnect.url, function(err) {
	if (err) {
		console.log(err);
	}else{
    console.log("Database Connection Initialized!");
    console.log("http://localhost:1995");
  }
});

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// Routers 
require('./server/routes/user.js')(app, server);

// Create Express Server Connection with port
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

