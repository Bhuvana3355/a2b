/**
 * @Filename 	: commonService.js
 * @Description : Business Logic for user activities.
 * @Author 		: Bhuvaneshwari B
 * @Date 		: Aug 02, 2020
 * 
 * 
 */

var registerDM 	= require('../models/user.js');
var log 		= require('../../server/config/logger').logger;
var appmsg 		= require('../config/message.js');
var config 		= require('../config/config.js');
var constants 	= require('../config/constants.js');
var path 		= require('path');
var crypto 		= require('crypto');
var nodemailer 	= require('nodemailer');
var filename 	= path.basename(__filename);

function saveOrUpdateUser(regObj, callback) {
	log.info(filename + '>>register>>');
	var response = {
		success	: Boolean,
		message	: String,
		data	: String
	};
	registerDM.find({ emailid: regObj.emailid}).exec(function (err, data) {
		log.info(filename + '>>register>>' + JSON.stringify(data));
		if(!data || data == ''){
			registerDM.create(regObj, function (err, data) {
				try {
					if (err) {
						response.success = false;
						response.message = appmsg.INTERNAL_ERR;
						callback(response);
					} else {
						response.success = true;
						response.message = appmsg.SAVE_SUCCESSFULLY;
						response.data = regObj;
						callback(response);
					}
				} catch (e) {
					log.info(e);
					callback(response);
				}
			});
		}else{
			response.success = false;
			response.message = appmsg.EXIST;
			callback(response);
		}
	});
}

function login(emailid, password, callback) {
	var response = {
		status	: Boolean,
		message	: String,
		data	: String
	};
	try{
		log.info(filename + '>>login>>');
		var shasum = crypto.createHash('sha1');
			shasum.update(password);
			var pwd = shasum.digest('hex');
		registerDM.find({ emailid	: emailid, password : pwd }).exec(function(error, result) {
			log.info('Result: ' + JSON.stringify(result));
			
			if(error) return callback(error);

			else if(!result || result == ''){
				log.info("Please Enter Valid Username OR Password");
				response.message = appmsg.LOGIN_ERR;
				response.status  = false;
				response.data    = result;
				callback(response);
				
			}else{
				// log.info("You have logged in successfully" + JSON.stringify(result));
				// log.info("You have logged in successfully" + JSON.stringify(result[0].password));
				if(pwd == result[0].password){
					log.info("You have logged in successfully");
					response.message = appmsg.LOGIN;
					response.status  = true;
					response.data    = result;
					callback(response);
				}else{
					log.info("Please Enter Valid Username OR Password");
					response.message = appmsg.LOGIN_ERR;
					response.status  = false;
					response.data    = result;
					callback(response);
				}
			}
		});
	}catch(e) {
		log.info('Internal Error');
		response.message = appmsg.INTERNAL_ERR;
		response.status  = false;
		callback(response);
	}
	
}

function sendMail(to, subjectbody, messagebody, callback){
	log.info(">>> Mail Triggered >>>");
	var response = {
		status	: Boolean,
		message	: String,
		data	: String
	};
	var transporter = nodemailer.createTransport({
		  host	: 'pod51009.outlook.com',
		  port	: 587,
		  secure: false, 
		  auth	: {
			user	: constants.AUTHMAILID,
			pass	: constants.AUTHMAILPWD
		  }
	});
 
	var mailOptions = {
		from	: constants.AUTHMAILID,
		to		: to,
		subject	: subjectbody,
		html	: messagebody
	};

	log.info(" >> mailOptions >>" + mailOptions);
	transporter.sendMail(mailOptions, function(error, info) {
		if (error) {
			log.info("Error: " + error);
			return callback(error);
		}else {
			log.info('Email sent: ' + JSON.stringify(info));
			log.info('Mail Triggered successfully');
			response.message = appmsg.MAIL_SUCCESS;
			response.data = info;
			response.status = true;
			return callback(response);
		}
	});
}

module.exports = {
	saveOrUpdateUser : saveOrUpdateUser,
	login 			 : login,
	sendMail		 : sendMail
};