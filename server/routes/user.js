/**
 * @Filename 	: 	user.js 
 * @Description : 	Business Logic for user activities. 
 * @Author		:	Bhuvaneshwari B
 * @Date		:   Aug 02, 2020
 * 
 * 
 */

var commonService = require('../services/commonService.js');
var log 		  = require('../../server/config/logger').logger;
var crypto 		  = require('crypto');
var ejs 		  = require('ejs');
var fs 			  = require('fs');


module.exports = function(app, server){
	function saveOrUpdateUser(req, res){
		var regObj={
				regid 				: req.param('regid'),
				emailid 			: req.param('emailid'),
				mobileno 			: req.param('mobileno'),
				role 			    : req.param('role'),
				username 			: req.param('username'),
				password  			: req.param('password'),
				status 				: req.param('status'),
				createdby 			: req.param('createdby'),
				createdtime 		: req.param('createdtime')		
		};
		var shasum = crypto.createHash('sha1');
		shasum.update(regObj.password);
		regObj.password	= shasum.digest('hex');
		
		commonService.saveOrUpdateUser(regObj, function(response, err){
			if(err){
				log.info(">>> Error >>>>" + err);
				res.send(err);
			}else{
				log.info(">>>> Response >>>>" + JSON.stringify(response));
				if(response.success == false){
					return res.send(response);
				}else{
					var templateString = fs.readFileSync(__dirname + '/../mailTemplates/registrationTemplate.ejs', 'utf-8');
					var tempdata = ejs.render(templateString, response.data);
					commonService.sendMail(response.data.emailid, 'Registration', tempdata, function (err, resp) {
						log.info(">>> Send Mail Response >>> ");
						if (err) {
							return res.send(err);
						} else {
							return res.send(resp);
						}
					});
				}
			}
		});
		
	}

	function login(req, res){
		var regObj = {
			emailid 			: req.param('emailid'),
			password  			: req.param('password')	
		};
		commonService.login(regObj.emailid, regObj.password, function(response, err){

			if(err){
				log.info(">>>>> Error >>>>>" + err);
				res.send(err);
			}else{
				log.info(">>>>> Response >>>>>" + JSON.stringify(response));
				res.send(response);
			}
		});
	}
	app.post('/register', saveOrUpdateUser);
	app.post('/login', login);
};
