/**
 * @Filename	:	logger.js
 * @Description	:	A logger object setting file to setup log file for the application.
 * @Author		:	Bhuvaneshwari B
 * @Date		:   Aug 02, 2020
 * 
 * 
 * 
 */

var logger = require('winston'); 									// used npm package winston
logger.add(logger.transports.File, { filename: './log/test.log' });	// set up a Logfile
exports.logger=logger;




