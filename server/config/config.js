/**
 * @File Name	:	config.js
 * @Description	:	config file.
 * @Author		:	Bhuvaneshwari B
 * @Date		:   Aug 02, 2020
 * 
 * 
 * 
 */

exports.PORT		 = 1995;
exports.SERVER		 = "http://localhost:1995";