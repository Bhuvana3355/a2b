/**
 * @Filename	:	dbService.js
 * @Description	:	Database params.
 * Author		:	Bhuvaneshwari B
 * Date			:	Jan 09, 2018
 * 
 * 
 * Version      Date           	Modified By             Remarks
 * 0.1 		 	Jan 09 			Bhuvaneshwari B	 
 */

module.exports = {
	'url' : 'mongodb://localhost:27017/sample'
};
