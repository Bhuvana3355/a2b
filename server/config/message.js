/**
 * @Filename	:	message.js
 * @Description	:	Constant messages for the application.
 * @Author		:	Bhuvaneshwari B
 * @Date		:   Aug 02, 2020
 * 
 * 
 */

exports.SAVE_SUCCESSFULLY='Saved Successfully';
exports.UPDATE_SUCCESSFULLY='Saved Successfully';
exports.INTERNAL_ERR="Internal error";
exports.LIST_NT_FOUND="List Not Found";
exports.LIST_FOUND="List Found";
exports.DATA_NT_FOUND="Data Not Found";
exports.DATA_FOUND="Data Found";
exports.EXIST = "Already registered, Please login with us.";
exports.LOGIN_ERR = "Please Enter Valid Username OR Password";
exports.LOGIN = "You have logged in successfully!";
exports.MAIL_SUCCESS = "Registration completed.... Mail Triggered.. Please check your mail for your reference!";
