/**
 * @Filename	:	register.js
 * @Description	:	Model Object For Registration.
 * @Author		:	Bhuvaneshwari B
 * @Date		:   Aug 02, 2020
 *  
 * 
 */

var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var userSchema = new Schema({
	
	regid 				: String,
	emailid 			: String,
	mobileno			: String,
	role				: String,
	username 			: String,
	password 			: String,
	status 				: String,
	createdby 			: String,
	createdtime 		: String

});
module.exports = mongoose.model("users", userSchema);